# DeskControl, a program to control desktop apps from a remote system.
# Copyright (C) Sean R. Lynch <seanl@literati.org>

# This program is free software: you can redistribute it and/or modify it under
# the terms of the GNU Affero General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option) any
# later version.

# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more
# details.

# You should have received a copy of the GNU Affero General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.

from setuptools import setup, find_packages

setup(
    name="DeskControl",
    version="0.2",
    packages=find_packages(),
    install_requires=['Pyro4', 'appdirs'],
    entry_points={
        'console_scripts': [
            'dc_client = deskcontrol.client:main',
            'dc_server = deskcontrol.server:main'
        ]
    },

    # metadata to display on PyPI
    author="Sean Lynch",
    author_email="seanl@literati.org",
    description="Control desktop applications over forwarded SSH connections",
    license="AGPL"
)

