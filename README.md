# DeskControl

A client and server to control desktop apps, usually over forwarded SSH ports.

## Requirements

Python 3.6 or greater. I've tested the client on 3.5.3 as well, but secret
generation requires the secrets module, so you'll need to manually generate a
hex cookie if you want to run the server on a Python older than 3.6. Setuptools
or Pip will install any other dependencies, mainly
[Pyro4](https://pythonhosted.org/Pyro4/), which has its own dependencies, and
[appdirs](https://pypi.org/project/appdirs/). It could probably be made to work
with Python 2.7 without too much effort, but you should be using Python 3.

DeskControl has been tested with Debian Stretch and Windows as the client and
Arch Linux and Windows 10 as the server. It should work on all platforms that
Python 3 and Pyro4 support. If it doesn't, that's a bug, so please let me know.

## Installation

Since this is a Setuptools package, you have several options for installing it.
I recommend installing either in your user Python directory or in a virtualenv.

From within the source directory, run `pip install --user .` or `pip3 install
--user .` depending on whether your default Python is Python 3. If you are
installing in a virtualenv, activate the virtualenv before installing and omit
`--user` from the pip command. You shouldn't need `pip3` with a virtualenv since
`pip` will always refer to the correct version.

Because DeskControl uses Setuptools's automatic script generation, the scripts
contain the full path to the Python interpreter, so they can be run directly out
of a virtualenv without activating it.

You can also install directly from Gitlab without downloading anything first.
Just use `pip install --user git+https://gitlab.com/seanl1/deskcontrol.git`.

## Usage

Install the package on the remote and local systems. Start the server
(dc_server) on the desktop system, providing it a port number to use. When
connecting to the remote system, forward some localhost port on that system to
the port you chose on the server. To open a web page, call `dc_client -p <port>
open <url>` on the remote system.

The first time the server is run, it will generate a secret to use as an HMAC
key, store that in its config file, and spit out a command to run on the client
to store the key (which we call a "cookie"). Unfortunately, the client and
server can each only handle one key at a time right now, so if you run the
software on multiple desktops (as the author does), you will need to manually
copy the config file so that every system shares the same key.

The config lives in 'literati.org/DeskConfig/config.ini' under the appropriate
app config dir on your platform: `%appdata%` on Windows,
`~/.local/config` or something on Linux, something else
on MacOS.

## Security

Messages between the client and server are validated using HMAC with a shared
secret. Security relies on that secret's staying secret. The software makes an
attempt to ensure that the config file is only readable by the user, but the
system administrator can read it. Do not try to run the client on machines on
which you do not trust the system administrator.

## To do

* Move the port from the command line to the config file
* Other operations besides opening URLs, such as syncing the clipboard
* Handle multiple different servers with different cookies
