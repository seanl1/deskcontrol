# DeskControl, a program to control desktop apps from a remote system.
# Copyright (C) Sean R. Lynch <seanl@literati.org>

# This program is free software: you can redistribute it and/or modify it under
# the terms of the GNU Affero General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option) any
# later version.

# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more
# details.

# You should have received a copy of the GNU Affero General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.

import configparser
from pathlib import Path
import os
import stat
import sys

import appdirs


_config_dir = Path(appdirs.user_config_dir('DeskControl', 'literati.org'))
_config_filename = _config_dir.joinpath('config.ini')

def read_config():
    if _config_filename.exists():
        with _config_filename.open('r') as fp:
            if sys.platform != 'win32':
                # Don't know how to check this on Windows and I don't think it's necessary
                mode = os.stat(fp.fileno()).st_mode
                if mode & stat.S_IROTH :
                    raise ValueError('Config file is readable by others')
                if mode & stat.S_IWOTH:
                    raise ValueError('Config file is writable by others')
                if mode & stat.S_IRGRP:
                    raise ValueError('Config file is readable by group')
                if mode & stat.S_IWGRP:
                    raise ValueError('Config file is writable by group')
            data = fp.read()
    else:
        data = ''

    config = configparser.ConfigParser()
    config.read_string(data)
    return config


def write_config(config):
    _config_dir.mkdir(mode=0o700, parents=True, exist_ok=True)
    # Create the file such that it's only readable by the user
    fd = os.open(str(_config_filename), flags=os.O_WRONLY|os.O_CREAT, mode=0o600)
    with os.fdopen(fd, 'w') as fp:
        config.write(fp)

