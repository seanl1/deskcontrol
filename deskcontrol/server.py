# DeskControl, a program to control desktop apps from a remote system.
# Copyright (C) Sean R. Lynch <seanl@literati.org>

# This program is free software: you can redistribute it and/or modify it under
# the terms of the GNU Affero General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option) any
# later version.

# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more
# details.

# You should have received a copy of the GNU Affero General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.

import binascii
import webbrowser

import Pyro4

from . import config


@Pyro4.expose
class BrowserController:
    def open_url(self, url):
        return webbrowser.open(url)


def main():
    from argparse import ArgumentParser

    parser = ArgumentParser()
    parser.add_argument('-p', '--port', type=int, help='Port number to listen on', required=True)

    args = parser.parse_args()

    conf = config.read_config()
    if 'server' not in conf:
        conf['server'] = {}

    if 'cookie' in conf['server']:
        hex_cookie = conf['server']['cookie']
    else:
        import secrets
        hex_cookie = str(secrets.token_hex(16))
        conf['server']['cookie'] = hex_cookie
        config.write_config(conf)
        print('Generated new cookie and wrote it to the config.')

    print('Cookie is {!r}'.format(hex_cookie))
    print('Set it with `dc_client set cookie {!r}` on the client.'.format(hex_cookie))
    cookie = binascii.a2b_hex(hex_cookie)
    daemon = Pyro4.core.Daemon(port=args.port)
    daemon._pyroHmacKey = cookie

    Pyro4.Daemon.serveSimple(
        {
            BrowserController: 'BrowserController'
        },
        daemon=daemon,
        ns=False
    )


if __name__ == '__main__':
    main()
