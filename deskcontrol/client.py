# DeskControl, a program to control desktop apps from a remote system.
# Copyright (C) Sean R. Lynch <seanl@literati.org>

# This program is free software: you can redistribute it and/or modify it under
# the terms of the GNU Affero General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option) any
# later version.

# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more
# details.

# You should have received a copy of the GNU Affero General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.

import binascii
import sys

import Pyro4

from . import config

_config_keys = ['cookie']


def main():
    from argparse import ArgumentParser

    parser = ArgumentParser()
    parser.add_argument('-p', '--port', type=int)
    ap = parser.add_subparsers(dest='command')
    subparser = ap.add_parser('open', description='Open a URL in the default browser')
    subparser.add_argument('url', help='URL to open')

    subparser = ap.add_parser('set', description='Set a config value')
    subparser.add_argument('key', help='Config key to set')
    subparser.add_argument('value', help='Value to set')

    args = parser.parse_args()

    conf = config.read_config()
    if 'client' not in conf:
        conf['client'] = {}

    if args.command == 'set':
        if args.key not in _config_keys:
            sys.exit('Valid keys: {!r}'.format(_config_keys))
        conf['client'][args.key] = args.value
        config.write_config(conf)
    elif args.command == 'open':
        uri = 'PYRO:BrowserController@localhost:{}'.format(args.port)
        cookie = binascii.a2b_hex(conf['client']['cookie'])
        browser = Pyro4.core.Proxy(uri)
        browser._pyroHmacKey = cookie
        browser.open_url(args.url)


if __name__ == '__main__':
    main()
